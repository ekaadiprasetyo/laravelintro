<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h3>Sign Up Form</h3>
    
    <form action="/selamat" method="post">
        @csrf
        <label>First Name:</label><br><br>
        <input type="text" name="fname"><br><br>
        <label>Last Name:</label><br><br>
        <input type="text" name="lname"><br><br>
        <label>Gender:</label><br><br>
        <input type="radio" name="gender" value="male" checked="checked">Male<br>
        <input type="radio" name="gender" value="female" checked="checked">Female<br>
        <input type="radio" name="gender" value="other" checked="checked">Other<br><br>
        <label for="">Nationality</label><br><br>
        <select name="Nationality">
            <option value="indonesia">Indonesia</option>
            <option value="malaysia">Malaysia</option>
            <option value="singapura">Singapura</option>
        </select><br><br>
        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="indonesia">Bahasa Indonesia<br>
        <input type="checkbox" name="english">English<br>
        <input type="checkbox" name="other">Other<br><br>
        <label>Bio:</label><br><br>
        <textarea name="" id="" cols="30" rows="10"></textarea>
        <br><br>
    </form>
    
    <!-- <input type="submit" value="Sign Up" href="selamat.html"> -->
    <a href="/selamat"><button>klik</button></a>
</body>
</html>